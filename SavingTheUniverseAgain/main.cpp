#include <iostream> // includes cin to read from stdin and cout to write to stdout
#include <vector>
#include <string>
using namespace std; // since cin and cout are both in namespace std, this saves some text


class Attack
{
private:
	bool _charge;
public:
	Attack(bool charge)
	{
		_charge = charge;
	}

	bool IsCharging()
	{
		return _charge;
	}
};

class AttackSequence
{
private:
	std::vector<Attack*> _attacks;
	int _totalDamage;
	int _attackPower;
	int _shield;
	int _hackedCount;

public:
	AttackSequence(int shield)
	{
		_shield = shield;
		_hackedCount = 0;
		_totalDamage = 0;
		_attackPower = 1;
	}

	AttackSequence(int shield, string line) : AttackSequence(shield)
	{
		for (unsigned int j = 0; j < line.size(); j++)
		{
			if (line[j] == 'C')
			{
				AddAttack(new Attack(true));
			}
			else
				AddAttack(new Attack(false));
		}
	}

	int AddAttack(Attack *attack)
	{
		_attacks.push_back(attack);
		if (attack->IsCharging())
		{
			_attackPower *= 2;
		}
		else
			_totalDamage += _attackPower;

		return _totalDamage;
	}

	bool Hack()
	{
		Attack *current = NULL;
		Attack *next = NULL;
		int charges = 0;
		for (unsigned int i = 0; i < _attacks.size() - 1; i++)
		{
			current = _attacks.at(i);
			next = _attacks.at(i + 1);

			if (current->IsCharging())
			{
				if (!next->IsCharging())
				{
					Swap(i);
					_totalDamage -= (1 + (charges * 1));
					_hackedCount++;
					if (_attacks.at(i + 1)->IsCharging() && (i == (_attacks.size() - 1)))
						_attacks.pop_back();
					return true;
				}
				else
					charges++;
			}
		}
		return false;
	}


	const std::string Solve()
	{
		bool success = true;
		while (_shield < _totalDamage)
		{
			success = Hack();
			if (!success)
				return "IMPOSSIBLE";
		}
		return std::to_string(_hackedCount);
	}

	// Changes the position of 2 adjacent elements
	// Position provided is of the leftmost element
	bool Swap(unsigned int position)
	{
		if (position > _attacks.size() - 1)
		{
			return false;
		}
		Attack *tmp = _attacks[position];
		_attacks[position] = _attacks[position + 1];
		_attacks[position + 1] = tmp;
		return true;
	}
};

int main() {
	int t, d;
	char p[30];
	cin >> t; // read t. cin knows that t is an int, so it reads it as such.
	for (int i = 1; i <= t; ++i) {
		cin >> d >> p; // read n and then m.
		AttackSequence *atkSeq = new AttackSequence(d, p);

		cout << "Case #" << i << ": " << (atkSeq->Solve()) << endl;
		// cout knows that n + m and n * m are ints, and prints them accordingly.
		// It also knows "Case #", ": ", and " " are strings and that endl ends the line.
	}
	return 0;
}
#include <iostream> // includes cin to read from stdin and cout to write to stdout
#include <vector>
#include <string>
using namespace std; // since cin and cout are both in namespace std, this saves some text


class TroubleSortList
{
private:
	std::vector<int> _list;

public:
	TroubleSortList()
	{		
	}

	void Add(int number)
	{
		_list.push_back(number);
	}

	unsigned int TroubleSort()
	{
		bool done = false;
		int index = -1;
		bool hasIndex = false;
		int indexedElement = NULL;
		while (!done)
		{
			done = true;
			for (unsigned int i = 0; i < _list.size() - 2; i++)
			{
				if (_list.at(i) > _list.at(i + 2))
				{
					done = false;
					std::swap(_list.at(i), _list.at(i + 2));
					if (i == index)
					{
						index = -1;
						hasIndex = false;
					}
							
				}
				if (_list.at(i + 1) > _list.at(i + 2))
				{
					if((!hasIndex) || ((i + 1) < index))
						index = i + 1;
					hasIndex = true;
				}
				if (_list.at(i) > _list.at(i + 1))
				{
					if ((!hasIndex) || (i < index))
						index = i;
					hasIndex = true;
				}
			}			
		}
		return index;
	}


	const std::string Solve()
	{
		int  result = TroubleSort();
		return (result == -1 ? "OK" : std::to_string(result));
	}
};

int main() {
	int t, n, vi;
	cin >> t; // read t. cin knows that t is an int, so it reads it as such.
	for (int i = 1; i <= t; ++i) {
		cin >> n; // read n
		TroubleSortList *tsList = new TroubleSortList();
		for (int j = 0; j < n; j++)
		{
			cin >> vi;
			tsList->Add(vi);
		}
		

		cout << "Case #" << i << ": " << (tsList->Solve()) << endl;
		// cout knows that n + m and n * m are ints, and prints them accordingly.
		// It also knows "Case #", ": ", and " " are strings and that endl ends the line.
	}
	return 0;
}